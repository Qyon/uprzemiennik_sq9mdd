/*
 * main.c
 *
 *  Created on: 09-06-2013
 *  Rewrite of SQ9MDD Bascom program
 *  Rewritten by Lukasz SQ5RWU
 *  License: GNU GPL
 *  In respect of HAM spirit.
 */

#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include "configuration.h"
#include "cw.h"


uint8_t Sendcwinterwal = 10; //czas automatycznego wysy�ania znamiennika w minutach
uint8_t Czaspodtrzymania; //
uint8_t SqlLevel;
uint8_t Rx;
uint8_t Rxa;
uint8_t Rx_flag;
uint8_t Podtrzymanie_flag;
uint8_t Odtworzylem_beep;
uint8_t Countdown;
uint8_t Beep_Uzywany;
uint8_t Sendcw = 1;
uint8_t Sendcw_uzywaj = 1;
uint8_t Sekunda;
uint8_t Minuta;

#if CZAS_PODTRZYMANIA_WENTYLATORA > 0
uint8_t PodtrzymujWentylator;
#endif

const char * Inputtekst = znamiennik;


inline void ustawCzasPodtrzymania() {
	uint8_t podtrzymanie = (~((PINB & (Podtrzymaniea | Podtrzymanieb | Podtrzymaniec)) >> 4))  & 0b111;
	if (podtrzymanie) {
		Czaspodtrzymania = podtrzymanie;
	} else {
		Czaspodtrzymania = 10; //sekund
	}
}

inline void ustawSqlLvl() {
	if (PINB & _BV(Sql_lvl)) {
		SqlLevel = _BV(Sql);
	} else {
		SqlLevel = 0;
	}
}

inline void ustawBeepanie() {
	if (PINB & _BV(Beep_option_jumper)) {
		Beep_Uzywany = 0;
	} else {
		Beep_Uzywany = 1;
	}
}

inline void ustawWysylanieZnamiennika() {
	if (PINB & _BV(Ident_option_jumper)) {
		//Sendcw_uzywaj = 0;
		Sendcw_uzywaj = 1;
	} else {
		Sendcw_uzywaj = 1;
	}
}

inline void enableTrx() {
	PORTB |= _BV(Ptt);
	PORTD |= _BV(Tx_led);
	PORTD |= _BV(Wentylator);
#if CZAS_PODTRZYMANIA_WENTYLATORA > 0
	PodtrzymujWentylator = CZAS_PODTRZYMANIA_WENTYLATORA;
#endif
}

void wylaczWentylator(){
	PORTD &= ~_BV(Wentylator);
}

inline void disableTrx() {
	//wy��czenie nadajnika
	PORTB &= ~_BV(Ptt);
	PORTD &= ~_BV(Tx_led);
#if CZAS_PODTRZYMANIA_WENTYLATORA > 0
	if (! PodtrzymujWentylator){
		PodtrzymujWentylator = CZAS_PODTRZYMANIA_WENTYLATORA;
	}
#else
	wylaczWentylator();
#endif
}



static inline void nadajZnamiennik() {
	enableTrx();
	//blokujemy timer na czas wysylania znamiennikabo chrypi
	TIMSK &= ~(1 << TOIE1);
	_delay_ms(300);
	sendCW(Inputtekst);
	//skonczylismy gasimy flaga nadawania znamiennika
	//resetujemy lflagi
	Sendcw = 0;
	//Ptt = 0;
	disableTrx();
	Sekunda = 0;
	//skonczylismy nadawac znamiennik odpalamy timer 1
	TIMSK |= (1 << TOIE1);
}

static inline void setup(){
	//wejscia konfiguracyjne port b portb.0 jako wyjscie PTT
	//wejsca sygnalowe i wyjscia sygnalizacyjne
	DDRB = 0b10000001;
	DDRD = 0b01111100;
	//ustawiamy wysokie stany na wejsciach i niskie na wyj�ciach
	PORTB = 0b11111110;
	PORTD = 0b00000000;

	ustawCzasPodtrzymania();
	ustawSqlLvl();
	ustawBeepanie();
	ustawWysylanieZnamiennika();

	initCw();

	// start timer1 with /64 prescaler
	TCCR1B = (1<<CS11) | (1<<CS10);
	TIMSK=(1<<TOIE1);
	sei();
}



static inline void loop(){
	//obs�uga sygna�u SQL zapalanie flagi RX
	//-------------------------------------------------------------------------------
	//jesli pin SQL ma stan inny ni� zworka ZW
	if ((PIND & _BV(Sql)) != SqlLevel){
		//ustawiam bit pomocniczy
		Rx = 1;
		//je�li po 20ms nadal wci�ni�ty uruchamiam zatrzask Rxa
		if (Rx == 1 && Rxa == 0){
			Rxa = 1;
			//ustawiamy flage jest RX
		    Rx_flag = 1;
		}
	}else if (Rxa == 1){
		//je�li sygna� zaj�to�ci znika to
		Rx_flag = 0;
		Rx = 0;
		Rxa = 0;
	}
	//ustawianie flagi TX jesli ustawione flagi RX lub podtrzymanie
	//-------------------------------------------------------------------------------

	//ustawianie flagi podtrzymanie jesli zapalony RX
	if (Rx_flag == 1) {
		Podtrzymanie_flag = 1;
		Odtworzylem_beep = 0;
		Countdown = 0;
		PORTD |= _BV(Rx_led);
		PORTD &= ~_BV(Mute);
	} else {
        PORTD &= ~_BV(Rx_led);
        PORTD |= _BV(Mute);
	}

	//ustawiamy port PTT i inne jesli mamy flage podtrzymanie
	if (Podtrzymanie_flag) {
		enableTrx();
		//tutaj beep jesli koniec nadawania i beep_option_jumper enabled  (0 to stan niski zworka za�o�ona)
		if (Rx_flag == 0 && Beep_Uzywany && Odtworzylem_beep == 0){
			_delay_ms(300);
			// sprawdzic czy stoi no�na je�li nie to beep
			if ((PIND & _BV(Sql)) == SqlLevel){
				beep(Kropka);
				Odtworzylem_beep = 1;
			}
		}
	} else {
		//wy��czenie nadajnika
		disableTrx();
	}

	//procedura wysylania znamiennika
	//-------------------------------------------------------------------------------
	//ident option jesli zwarty do masy to b�dzie nadawany znamiennik
	if (Sendcw && Sendcw_uzywaj) {
		nadajZnamiennik();
	}
	// koniec p�tli
}




ISR(TIMER1_OVF_vect) {
	TCNT1=0x00;
	if (PodtrzymujWentylator) {
		PodtrzymujWentylator--;
		if (PodtrzymujWentylator <= 0){
			wylaczWentylator();
		}
	}

	//tutaj proste naliczanie w sekundzie i cos (dla uproszczenia i skrocenia kodu uzy�em timera tc1/64 to daje 1s = 1,05S)
	Sekunda++;
	if (Sekunda > 59){
		Minuta++;
		if (Minuta > 59){
			Minuta = 0;
		}
		Sekunda = 0;
	}

	//jesli podtrzymanie na 1 a rx spadl zaczynamy odliczanie do wylaczenia
	if (Podtrzymanie_flag && ~Rx_flag){
		Countdown++;
		if (Countdown >= Czaspodtrzymania) {
			Podtrzymanie_flag = 0;
			disableTrx();
			//jesli zaznaczona flaga identylfikacji po odpuszczeniu tx to zagraj znamiennik
			if (Sendcw_uzywaj){
				Minuta = Sendcwinterwal;
			}
		}
	} else if(Podtrzymanie_flag && Rx_flag) {
		//reset licznika jesli pojawi sie flaga rx w trakcie podtrzymania
		Countdown = 0;
	}
	//okresowe wysy�anie znamiennika jesli przekroczono czas i nikt nie nadaje
	if (Minuta >= Sendcwinterwal && ~Rx_flag && Sendcw_uzywaj){
		Sendcw = 1;
		Minuta = 0;
	}
}


int main(){
	setup();
	while(1){
		loop();
	}
}
