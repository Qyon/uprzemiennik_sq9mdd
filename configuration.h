/*
 * configuration.h
 *
 *  Created on: 09-06-2013
 *  Rewrite of SQ9MDD Bascom program
 *  Rewritten by Lukasz SQ5RWU
 *  License: GNU GPL
 *  In respect of HAM spirit.
 */

#ifndef CONFIGURATION_H_
#define CONFIGURATION_H_
#include <avr/io.h>
//aliasy na porcie D
#define Sql PD1
#define Morsepin PD6
#define Tx_led PD5
#define Rx_led PD4
#define Mute PD3
#define Wentylator PD2
//aliasy na porcie B
#define Ptt PB0
#define Beep_option_jumper PB1
#define Ident_after_jumper PB2
#define Ident_option_jumper PB3
#define Podtrzymaniea PB4
#define Podtrzymanieb PB5
#define Podtrzymaniec PB6
#define Sql_lvl PB7
//definicje sta�ych
#define Czestotliwosc 450 // Hz
#define Kropka 100 // ms
#define Kreska (3 * Kropka)

#define znamiennik "SR5WM"

// czas porzymania pracy wentylatora po skonczeniu nadawania w sekundach
#define CZAS_PODTRZYMANIA_WENTYLATORA 20

#endif /* CONFIGURATION_H_ */
