/*
 * cw.c
 *
 *  Created on: 09-06-2013
 *  Rewrite of SQ9MDD Bascom program
 *  Rewritten by Lukasz SQ5RWU
 *  License: GNU GPL
 *  In respect of HAM spirit.
 */

#include "cw.h"
#include <avr/pgmspace.h>

const uint16_t HalfPeriodUs = 62500/Czestotliwosc;

cw_char PROGMEM morse[] = {
		{ 5, 0b11111000 }, //0
		{ 5, 0b01111000 }, //1
 		{ 5, 0b00111000 }, //2
 		{ 5, 0b00011000 }, //3
 		{ 5, 0b00001000 }, //4
 		{ 5, 0b00000000 }, //5
 		{ 5, 0b10000000 }, //6
 		{ 5, 0b11000000 }, //7
 		{ 5, 0b11100000 }, //8
 		{ 5, 0b11110000 }, //9
 		{ 1, 0b00000000 }, //:
 		{ 1, 0b00000000 }, //;
 		{ 1, 0b00000000 }, //<
 		{ 1, 0b00000000 }, //=
 		{ 1, 0b00000000 }, //>
 		{ 1, 0b00000000 }, //?
 		{ 1, 0b00000000 }, //@
 		{ 2, 0b01000000 }, //A
 		{ 4, 0b10000000 }, //B
 		{ 4, 0b10100000 }, //C
 		{ 3, 0b10000000 }, //D
 		{ 1, 0b00000000 }, //E
 		{ 4, 0b00100000 }, //F
 		{ 3, 0b11000000 }, //G
 		{ 4, 0b00000000 }, //H
 		{ 2, 0b00000000 }, //I
 		{ 4, 0b01110000 }, //J
 		{ 3, 0b10100000 }, //K
 		{ 4, 0b01000000 }, //L
 		{ 2, 0b11000000 }, //M
 		{ 2, 0b10000000 }, //N
 		{ 3, 0b11100000 }, //O
 		{ 4, 0b01100000 }, //P
 		{ 4, 0b11010000 }, //Q
 		{ 3, 0b01000000 }, //R
 		{ 3, 0b00000000 }, //S
 		{ 1, 0b10000000 }, //T
 		{ 3, 0b00100000 }, //U
 		{ 4, 0b00010000 }, //V
 		{ 3, 0b01100000 }, //W
 		{ 4, 0b10010000 }, //X
 		{ 4, 0b10110000 }, //Y
 		{ 4, 0b11000000 }, //Z
};


void sendCWChar(char znak){
	cw_char znak_cw;
	memcpy_P(&znak_cw, &morse[znak - '0'], sizeof(cw_char));
	int8_t left = znak_cw.length;
	uint8_t data = znak_cw.data;

	do{
		if (data & 0b10000000){
			beep(Kreska);
		} else {
			beep(Kropka);
		}
		data = data << 1;
		_delay_ms(Kropka);
	} while(--left);
	_delay_ms(Kreska);
}

void sendCW(char* text) {
	char* znak = text;
	do {
		sendCWChar(znak[0]);
		znak++;
	} while (znak[0]);
}
volatile uint8_t _beep_temp = 0;
inline void beep(uint16_t time_ms){
	_beep_temp = 0;
	TCNT0=HalfPeriodUs;
	TIMSK|=(1<<TOIE0);
	do {
		_delay_ms(1);
	}while(--time_ms); // optymalizacja - nie uzywamy dzieki temu float�w!
	TIMSK&=~(1<<TOIE0);
	PORTD &= ~_BV(Morsepin);
}

void initCw(){
#if defined (__AVR_ATtiny2313__)
TCCR0A|= (1 << CS01); // :8
#else
TCCR0 |= (1 << CS01); // :8
#endif
}

ISR(TIMER0_OVF_vect) {
	TCNT0=HalfPeriodUs;
	if (_beep_temp < 2){
		PORTD |= _BV(Morsepin);
	} else {
		PORTD &= ~_BV(Morsepin);
	}
	_beep_temp++;
	if(_beep_temp>=4){
		_beep_temp = 0;
	}
}

