/*
 * cw.h
 *
 *  Created on: 09-06-2013
 *  Rewrite of SQ9MDD Bascom program
 *  Rewritten by Lukasz SQ5RWU
 *  License: GNU GPL
 *  In respect of HAM spirit.
 */

#ifndef CW_H_
#define CW_H_

#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include "configuration.h"

void sendCW(char* Inputtekst);
void beep(uint16_t time);
void initCw();

typedef struct  {
	uint8_t length;
	uint8_t data;
} cw_char;
#endif /* CW_H_ */
